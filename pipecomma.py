#!/usr/bin/env python3

from macro import macro


@macro
def pipecomma(code: str) -> str:
    return code.replace("|", ",")


pipecomma or print("Hello" | "World")

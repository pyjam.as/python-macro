
# Macros in python

Turns out you can do runtime code manipulation in python by using `inspect` and short-circuiting operators.

I implemented a decorator `@macro` that lets you make string-to-string code manipulation in python.

It's at best prototype-level quality right now. Lots of stuff doesn't work.

## Usage

```py
from macro import macro

@macro
def pipecomma(code: str) -> str:
    return code.replace("|", ",")

pipecomma or print("Hello" | "World")
```

This code just translates `print("Hello" | "World")` into `print("Hello", "World")`.

## Motivation

When print-debugging python, I have gotten into the habit of writing print statements like:

```py
print(f"{my_variable=}")
```

Which, if you don't know, will print a string like `f"my_variable={my_variable}"`. Super useful. Very verbose.

Like: `(f"{` and `=}")`? Really?

And even worse, print will just discard the value. So if you need to use it somewhere, you need to refer to it twice.

You end up with code like:

```py
four = 2 + 2
print(f"{four=}")
do_my_thing(four)
```
...which prints `four=4`

I'd much prefer to have a function (macro?) that works like the [`dbg!`](https://doc.rust-lang.org/std/macro.dbg.html) macro in Rust.

```py
do_my_thing(dbg(2 + 2))
```
...which prints `2 + 2 = 4`

A couple of months ago I concluded that python was incapable of this, and wrote my thing in [hy](http://hylang.org/) instead.

My hy macro is [here](https://gitlab.com/AsbjornOlling/hy-dbg-macro).

But it turns out, python *can* do it!

## Credit where credit is due

Props my friend Vincent Olesen for coming up with this idea.

He originally wrote a thing that lets you write email addresses directly in python like: `Email or alice@example.org`

I just copied his idea and made a shitty macro library of it.

## How it works

Short circuiting operators (like `or`) won't evaluate the right-hand side of the operator if it doesn't have to.

So as long as the left-hand side of an `or` is `True`, you can write anything **that parses** on the right hand side. So you can write `mymacro or 2 - "wow" / True + undefined_var`, but not `mymacro or =/-*`.

The `inspect` module allows us to get the filename and linenumber of the call-site, which we can use to extract the source code text.

That's pretty much it.

## Other short-circuiting ops

You can do similar stuff with `and` or `>`

It would be cool to implement these as well. Should they have different features?

## Eval vs exec

`eval` will only evaluate one expression, and it doesn't support statements (like assignments, loops, conditionals, etc)- BUT it does return the value of the expression.

`exec` does support multiple lines of code- expressions and statements alike. But it's a bit tricky to get the value of out. This was the original reason for writing the `d()or` macro.

Maybe `or` macros should use `exec`, while `and` macros use eval? Hmm...

## What doesn't work

Lots of stuff! Feel free to contribute fixes if you feel like it.

At least a couple of these should be trivial to implement.

A short list of deficiencies:

- Multiple-line macros
- More than one macro on the same line
- Nested macros
- Macros with "or" in the name
- Macros on lines with an "or" before the macro
- Using `locals` from the call-site scope
- Calling macros inside macros (just need to implement `Macro.__call__`)
- Macros in a REPL
- Macros in files outside of the current working dir

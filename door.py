#!/usr/bin/env python3

from macro import macro


@macro
def door(some_code: str, frame) -> str:
    # split on <-
    name, code = some_code.split("<-")

    # eval the code, assign to name in calling scope
    frame.f_locals[name.strip()] = eval(code.strip())

    # TODO: make this macro return code instead of just running it
    #       this would let us chain it, and use it inside other macros
    return "None"


def d():
    return door

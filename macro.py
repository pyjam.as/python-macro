#!/usr/bin/env python3

import re
import inspect


class Macro:
    def __init__(self, f):
        self.f = f

    def __bool__(self):
        """This is where the magic happens"""
        frame = inspect.currentframe().f_back

        # TODO: more robust way of finding file
        filename = frame.f_code.co_filename
        lineno = frame.f_lineno

        with open(filename, "r") as f:
            source_line = list(f)[lineno - 1]

        # XXX: lol this is not robust at all

        # strip everything until the first or
        pattern = f"^.*?or"
        macroinput = re.sub(pattern, "", source_line).strip()

        macroparams = inspect.signature(self.f).parameters
        if len(macroparams) == 2:
            macrooutput = self.f(macroinput, frame)
        else:
            macrooutput = self.f(macroinput)

        # TODO: how to get result from here?
        exec(macrooutput)

        return True


def macro(f):
    return Macro(f)


@macro
def identity(some_code: str) -> str:
    new_code = some_code
    return new_code


